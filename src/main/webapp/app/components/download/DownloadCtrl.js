/**
 * Created by Simon on 2016-08-21.
 */

angular.module('downloadComponent',['ui.router'])
    .controller('downloadCtrl',['$scope','searchService', function($scope,searchService) {

        $scope.errors = {
            showErrors: false,
            validQuery: true
        };

        $scope.attributes = {
            query: null,
            statistic: null
        };

        $scope.validateForm = function(form){
            $scope.errors.showErrors = true;

            if(!form.$valid)
                return;

            searchService.searchNotation($scope.attributes).
                then(function(response){
                });
        }
    }]);