/**
 * Created by Simon on 2016-07-30.
 */

angular.module('app')
.controller('RegistrationCtrl',['$scope','$rootScope' ,'$uibModalInstance','$window','registrationService', function ($scope,$rootScope,$uibModalInstance,$window,registrationService) {

    $scope.defaultDate = new Date(9999,1,1); //default day
    $scope.dt = new Date(1993,1,1);
    $scope.format = 'dd-MMMM-yyyy';

    /** Registered new account information **/
    $scope.validatePassword = validatePassword;
    $scope.validateResponse = validateResponse;
    $scope.setSuccessAlert = setSuccessAlert;
    $scope.account = {
        username: null,
        password: null,
        confirmedPassword: null,
        email: null,
        birthday: null
    };

    $scope.errors = {
        showErrors: false,
        passValidated: false,
        isUsernameUnique: true,
        isEmailUnique: true
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(),
        minDate: new Date(1900,1,1),
        startingDay: 1
    };

    function validatePassword(){
        if($scope.account.password != $scope.account.confirmedPassword)
            $scope.errors.passValidated = false;
        else
            $scope.errors.passValidated = true;
    };

    function validateResponse(data){
        $scope.errors.isUsernameUnique = data.usernameError != null ? false : true;
        $scope.errors.isEmailUnique    = data.emailError    != null ? false : true;
    }

    function setSuccessAlert(){
        $rootScope.showAlert = true;
        $rootScope.type = 'alert-success';
        $rootScope.alertMsg = 'Your registration has been successfully completed';
    }

    /**********Submiting form**********/

    $scope.open = function() {
        $scope.popup.opened = true;
    };

    $scope.popup = {
        opened: false
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.closeModal = function () {
        $uibModalInstance.close();
    };

    $scope.submit = function(form){
        console.log('submiting....');

        if(form.$valid) {
            $scope.account.birthday = $scope.dt;
            registrationService.validateUniqueFields($scope.account)
                .then(function success(response){
                    if(response.data.hasErrors)
                        validateResponse(response.data);
                    else {
                        console.log('Registration completed!');
                        $scope.closeModal();
                        $scope.setSuccessAlert();
                        //Log in automatically
                    }
                });
        }
    };

}]);