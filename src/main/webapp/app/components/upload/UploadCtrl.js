/**
 * Created by Simon on 2016-08-21.
 */

angular.module('uploadComponent',['ui.router'])
    .controller('uploadCtrl',['$scope','uploadService','uploadFileService' ,function($scope,uploadService,uploadFileService){

        /* Data transfer object */
        $scope.notation = {
            user: 'Dagothversur',
            name: null,
            encryptionRequested: false
        };
        $scope.selectedFile = null;
        $scope.content = null;

        $scope.validateForm = function(){
            console.log('Validatation...');

            uploadFileService.sendNotation($scope.content,$scope.notation).
                then(function(response){
                    console.log(response);
            });
        };

        $scope.uploadFile = function(event){
            $scope.content = event.target.files[0];

            var fileName = $scope.content.name;
            $scope.selectedFile = fileName;
            $scope.$apply();
        };

    }]);