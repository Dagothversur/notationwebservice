/**
 * Created by Simon on 2016-08-29.
 */

angular.module('services',[])
    /* Services */

    .service('uploadService',['$http', function($http){

        this.sendNotation = function(uploadData){

            return $http({
                method: 'POST',
                url: 'http://localhost:8081/NotationWebService-1.0-SNAPSHOT/uploadNotation',
                data: uploadData
            });
        };
    }])
    .service('registrationService',['$http',function($http){

        this.validateUniqueFields = function(dataToValidate){
            return $http({
                method: 'POST',
                url: 'http://localhost:8081/NotationWebService-1.0-SNAPSHOT/getUniqueFieldStatus',
                data: {
                    username: dataToValidate.username,
                    email: dataToValidate.email,
                    birthday: dataToValidate.birthday,
                    password: dataToValidate.password,
                }
            });
        }
    }])
    .service('uploadFileService',['$http', function($http){

        this.sendNotation = function(uploadFile,notationDto){

            var formData = new FormData();
            var uploadUrl = 'http://localhost:8081/NotationWebService-1.0-SNAPSHOT/uploadNotation';

            formData.append('file', uploadFile);
            formData.append('notationDto', new Blob([JSON.stringify({
                "user": notationDto.user,
                "name": notationDto.name,
                "encryptionRequested": notationDto.encryptionRequested
            })], {
                type: "application/json"
            }));

            return $http.post(uploadUrl, formData, {
                withCredentials: false,
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: angular.identity
            })
        };
    }])
    .service('searchService',['$http',function($http){

        this.searchNotation = function(searchData){

            return $http({
                method: 'POST',
                url: 'http://localhost:8081/NotationWebService-1.0-SNAPSHOT/searchNotation',
                data: searchData
            });

        };
    }])
    /* Directives */
    .directive('customOnChange',function() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var onChangeHandler = scope.$eval(attrs.customOnChange);
            element.bind('change', onChangeHandler);
        }
    };

});