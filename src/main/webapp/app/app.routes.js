/**
 * Created by Simon on 2016-07-24.
 */

/** Url State Provider **/
angular.module('app')
    .config(function($stateProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('upload',{
                url: '/upload',
                templateUrl: 'app/components/upload/upload.html',
                controller: 'uploadCtrl'
            })
            .state('download',{
                url:'/download',
                templateUrl: 'app/components/download/download.html',
                controller: 'downloadCtrl'
            })
            .state('settings',{
                url:'/settings',
                templateUrl: 'app/components/settings/settings.html',
                controller: 'settingsCtrl'
            });
    });