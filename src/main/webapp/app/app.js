/**
 * Created by Simon on 2016-07-24.
 */

// 'settingsComponent','downloadComponent','uploadComponent'
/** Main Dependencies **/
angular.module('app',['ui.bootstrap','ngMessages','ui.router','services','uploadComponent','settingsComponent','downloadComponent']);
angular.module('app')
    .controller('HomePageController', ['$scope','$uibModal','$location','$rootScope', function ($scope,$uibModal,$location,$rootScope)
    {

        $scope.title = 'Welcome to Notation Service';
        $scope.isLogged = false;
        $rootScope.showAlert = false;
        $rootScope.type = 'alert-success';
        $rootScope.alertMsg = 'defaultAlert';
        $scope.account = {
            login: '',
            password: ''
        };

        $scope.isActive = isActive;
        $scope.signIn = signIn;
        $scope.signUp = signUp;
        $scope.forgotDetails = forgotDetails;
        $scope.closeAlert = function() {
            $rootScope.showAlert = false;
        };


        /** Opening registration modal **/
        function signUp(){
            console.log('New user signing up');
            var modalInstance = $uibModal.open({
                templateUrl: 'app/components/registrationModal/registration.html',
                controller: 'RegistrationCtrl'
            });

        }

        function isActive(destination){
            return destination === $location.path();
        }

        function signIn(){
            $scope.isLogged = true;
        }

        function forgotDetails(){
            console.log("Forgot details");
            var modalInstance = $uibModal.open({
                templateUrl: 'app/components/passwordRecovery/passwordRecovery.html',
                controller: 'PasswordRecoveryCtrl'
            });
        }

    }]);
