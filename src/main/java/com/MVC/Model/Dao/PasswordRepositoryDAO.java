package com.MVC.Model.Dao;

import com.MVC.Model.entity.Password;
import com.MVC.Model.repository.PasswordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Simon on 2016-10-24.
 */

@Repository
public class PasswordRepositoryDAO {

    @Autowired
    private PasswordRepository passwordRepository;

    public void persistPassword(Password password){
        passwordRepository.save(password);
    }

}
