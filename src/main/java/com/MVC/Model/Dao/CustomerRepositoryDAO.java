package com.MVC.Model.Dao;

import com.MVC.Model.entity.Customer;
import com.MVC.Model.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/*
 * Created by Simon on 2016-10-05.
 */
@Repository
public class CustomerRepositoryDAO {

    @Autowired
    private CustomerRepository customerRepository;

    public List<Customer> findAll(){
        return customerRepository.findAll();
    }

    public Customer findByUsername(String username){
        return customerRepository.findByUsername(username);
    }

    public void persistCustomer(Customer customer){
        customerRepository.save(customer);
    }
}
