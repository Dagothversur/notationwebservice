package com.MVC.Model.Dao;

import com.MVC.Model.entity.Details;
import com.MVC.Model.repository.DetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Simon on 2016-10-03.
 */
@Repository
public class DetailsRepositoryDAO {

    @Autowired
    private DetailsRepository detailsRepository;

    public Details findDetailsByEmail(String email){
        return detailsRepository.findByEmail(email);
    }

    @Transactional
    public void persistDetails(Details details){
        detailsRepository.save(details);
    }
}
