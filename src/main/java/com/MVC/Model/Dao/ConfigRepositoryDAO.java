package com.MVC.Model.Dao;

import com.MVC.Model.entity.Config;
import com.MVC.Model.repository.ConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Szymon on 22.11.2016.
 */
@Repository
public class ConfigRepositoryDAO {

    @Autowired
    private ConfigRepository configRepository;

    public Config getSystemConfig(){
        return configRepository.findOne(1L);
    }
}
