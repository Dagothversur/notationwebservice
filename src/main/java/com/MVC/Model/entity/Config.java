package com.MVC.Model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Szymon on 21.11.2016.
 */

@Entity
@Table(name = "CONFIG")
@Data
@NoArgsConstructor
public class Config {

    @Id
    private Long id;

    @Column(name = "DIRECTORY")
    private String directory;

    @Column(name = "FILE_EXTENSION")
    private String fileExtension;

    @Column(name = "FILE_SIZE")
    private Long fileMaxSize;
}
