package com.MVC.Model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Simon on 2016-10-03.
 */

@Entity
@Table(name = "DETAILS")
@Data
@NoArgsConstructor
public class Details {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO  , generator = "DETAILS_SEQ_GENERATOR")
    @SequenceGenerator(allocationSize = 1, name = "DETAILS_SEQ_GENERATOR", sequenceName = "DETAILS_SEQ")
    private Long id;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "BIRTHDAY")
    private Date birthday;

    public Details(String email, Date birthday){
        this.email = email;
        this.birthday = birthday;
    }
}
