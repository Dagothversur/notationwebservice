package com.MVC.Model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by Simon on 2016-10-05.
 */

@Entity
@Table(name = "CUSTOMER")
@Data
@NoArgsConstructor
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUSTOMER_SEQ_GENERATOR")
    @SequenceGenerator(allocationSize = 1, name = "CUSTOMER_SEQ_GENERATOR", sequenceName = "CUSTOMER_SEQ")
    private Long id;

    @Column(name = "USERNAME")
    private String username;

    @OneToOne
    @JoinColumn(name = "DETAILS_ID")
    private Details details;

    @OneToOne
    @JoinColumn(name = "PASSWORD_ID")
    private Password password;
}
