package com.MVC.Model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by Simon on 2016-10-05.
 */

@Entity
@Table(name = "PASSWORD")
@Data
@NoArgsConstructor
public class Password {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PASSWORD_SEQ_GENERATOR")
    @SequenceGenerator(allocationSize = 1,name = "PASSWORD_SEQ_GENERATOR", sequenceName = "PASSWORD_SEQ")
    private Long id;

    @Column(name = "PASSWORD")
    private String password;

    public Password(String password){
        this.password = password;
    }
}
