package com.MVC.Model.repository;

import com.MVC.Model.entity.Details;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Simon on 2016-10-03.
 */

public interface DetailsRepository extends CrudRepository<Details,Long>{

    Details findByEmail(String email);
}
