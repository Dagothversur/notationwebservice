package com.MVC.Model.repository;

import com.MVC.Model.entity.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Simon on 2016-10-05.
 */
public interface CustomerRepository extends CrudRepository<Customer,Long> {

    Customer findByUsername(String username);
    List<Customer> findAll();

}
