package com.MVC.Model.repository;

import com.MVC.Model.entity.Config;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Szymon on 21.11.2016.
 */
public interface ConfigRepository extends CrudRepository<Config,Long> {

}
