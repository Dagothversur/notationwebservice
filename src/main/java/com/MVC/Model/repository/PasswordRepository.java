package com.MVC.Model.repository;

import com.MVC.Model.entity.Password;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Simon on 2016-10-24.
 */
public interface PasswordRepository extends CrudRepository<Password,Long> {
}
