package com.MVC.Functionalities.encryption;

import com.MVC.Services.IOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Szymon on 27.11.2016.
 */

/* 1: Encryption boolean =  true
   2: Input: parafraza
   3: Szyfrujemy plik AES i szyfrujemy klucz
   4: Dajemy public key:
   5: zeby odszyfrowac plik customer podaje klucz ktorym odszyfrowujemy parafraze i uzywamy key to dekrypcji
* */

@Service
public class EncryptionService {

    private static final String ALGORITHM = "AES";
    private final static String TRANSFORMATION = "AES";

    public void encryptFile(String key,String inputPath, String outputPath) throws IOException
    {
        File inputFile = new File(inputPath);
        File outputFile = new File(outputPath);

        crypto(Cipher.ENCRYPT_MODE, key, inputFile, outputFile);
    }

    public void decryptFile(String key,String inputPath, String outputPath) throws IOException
    {
        File inputFile = new File(inputPath);
        File outputFile = new File(outputPath);

        crypto(Cipher.DECRYPT_MODE, key, inputFile, outputFile);
    }

    private void crypto(int cipherMode, String key, File inputFile, File outputFile) throws IOException {

        try {
            Key secretKey = new SecretKeySpec(key.getBytes(), ALGORITHM);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(cipherMode, secretKey);

            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);

            byte[] outputBytes = cipher.doFinal(inputBytes);

            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);

            inputStream.close();
            outputStream.close();
        }
        catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | IOException ex)
        { }
    }
}
