package com.MVC.Functionalities.passwordRecovery;

import com.MVC.Dto.PasswordRecoveryDto;
import com.MVC.Model.Dao.CustomerRepositoryDAO;
import com.MVC.Services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Simon on 2016-10-31.
 */
@Service
public class PasswordRecoveryService {

    @Autowired
    private MailService mailService;

    @Autowired
    private CustomerRepositoryDAO customerRepositoryDAO;

    public void processMailData(PasswordRecoveryDto passwordRecoveryDto){
        String to = passwordRecoveryDto.getEmail();
        String content = getMailContent(passwordRecoveryDto.getUsername());
        mailService.sendPasswordRecoveryMail(to,content);
    }

    private String getMailContent(String username){
        String header = "Hello, Request for remainding login and password remainding has been made. Details are to be found below.\n ";
        String userPassword = customerRepositoryDAO.findByUsername(username).getPassword().getPassword();

        return  header + "Username: " + username + "\n" + "Password: " + userPassword;
    }

}
