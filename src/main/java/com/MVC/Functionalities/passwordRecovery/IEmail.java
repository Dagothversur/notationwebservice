package com.MVC.Functionalities.passwordRecovery;

/**
 * Created by Simon on 2016-10-31.
 */
public interface IEmail {

    void setDestinationMail(String destinationMail);
    void setSubject(String subject);
    void setMailContent(String mailContent);

    String getDestinationMail();
    String getSubject();
    String getMailContent();
}
