package com.MVC.Functionalities.registration;

import com.MVC.Dto.RegistrationFieldsDto;
import com.MVC.Dto.RegistrationResponseDto;
import com.MVC.Model.Dao.CustomerRepositoryDAO;
import com.MVC.Model.Dao.DetailsRepositoryDAO;
import com.MVC.Model.Dao.PasswordRepositoryDAO;
import com.MVC.Model.entity.Customer;
import com.MVC.Model.entity.Details;
import com.MVC.Model.entity.Password;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.*;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Simon on 2016-10-04.
 */
@Service
public class RegistrationService {

    @Autowired
    private CustomerRepositoryDAO customerRepositoryDAO;

    @Autowired
    private DetailsRepositoryDAO detailsRepositoryDAO;

    @Autowired
    private PasswordRepositoryDAO passwordRepositoryDAO;

    /* Registration fields dto */
    private static final List<String> REG_FIELDS = Arrays.asList("username","email","password","birthday");

    /* Register account */
    @Transactional
    public void createNewAccount(RegistrationFieldsDto registrationFieldsDto){
        /* Creating persistence objects */
        Customer customer = new Customer();
        Details details = new Details(registrationFieldsDto.getEmail(),registrationFieldsDto.getBirthday());
        Password password = new Password(registrationFieldsDto.getPassword());

        customer.setDetails(details);
        customer.setPassword(password);
        customer.setUsername(registrationFieldsDto.getUsername());

        /* Persist new account */
        detailsRepositoryDAO.persistDetails(details);
        passwordRepositoryDAO.persistPassword(password);
        customerRepositoryDAO.persistCustomer(customer);
    }

    /* Create ResponseDto */
    public RegistrationResponseDto createResponse(Errors errors){
        RegistrationResponseDto responseDto = new RegistrationResponseDto();
        responseDto.setHasErrors(true);

        for(String currField: REG_FIELDS){
            String error = getErrorMsg(currField,errors);
            setFieldError(responseDto,currField,error);
        }

        return responseDto;
    }

    private String getErrorMsg(String field,Errors errors){
         FieldError fieldError = errors.getFieldError(field);
         if(fieldError != null)
             return fieldError.getDefaultMessage();

         return null;
    }

    private void setFieldError(RegistrationResponseDto responseDto,String field,String error){

        if(error != null) {
            if (field.equals("username"))
                responseDto.setUsernameError(error);
            if (field.equals("email"))
                responseDto.setEmailError(error);
            if (field.equals("password"))
                responseDto.setPasswordError(error);
            if (field.equals("birthday"))
                responseDto.setBirthdayError(error);
        }
    }
}
