package com.MVC.Functionalities.Search;

import lombok.Data;

import java.util.List;

/**
 * Created by Szymon on 08.12.2016.
 */

@Data
public class Statistic {

    private Integer docId;
    private String termMatched;
    private List<Integer> positions;

    public Statistic(Integer docId, String termMatched, List<Integer> positions){
        this.docId = docId;
        this.termMatched = termMatched;
        this.positions = positions;
    }
}
