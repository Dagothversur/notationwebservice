package com.MVC.Functionalities.Search;

import com.MVC.Dto.SearchDto;
import com.MVC.Services.IOService;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttributeImpl;
import org.apache.lucene.codecs.TermVectorsFormat;
import org.apache.lucene.codecs.TermVectorsReader;
import org.apache.lucene.codecs.compressing.CompressingTermVectorsFormat;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Attribute;
import org.apache.lucene.util.AttributeSource;
import org.apache.lucene.util.BytesRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Szymon on 23.11.2016.
 */

@Service
public class SearchService {

    @Autowired
    private IOService ioService;

    private final static String SEARCH_FIELD = "text";

    public String performSearch(String username,SearchDto searchDto) throws IOException, ParseException {

        List<Statistic> statistics = search(username,searchDto);
        String path = ioService.createStatisticFile(username,statistics);

        return path;
    }


    private List<Statistic> search(String username, SearchDto searchDto) throws IOException, ParseException {

        List<Statistic> statistics = new ArrayList<>();

        /* Index Reader */
        Pattern pattern = getPattern(searchDto.getQuery());
        IndexReader indexReader = getIndexReader(ioService.getIndexDir(username));

        for(int i = 0; i<indexReader.maxDoc(); i++) {

            Document doc = indexReader.document(i);

            Terms terms = indexReader.getTermVector(i, SEARCH_FIELD);
            TermsEnum iterator = terms.iterator();

            BytesRef byteRef = null;
            PostingsEnum postingsEnum = null;

            while ((byteRef = iterator.next()) != null) {
                String term = byteRef.utf8ToString();

                postingsEnum = iterator.postings(postingsEnum);
                postingsEnum.nextDoc();

                int termFreq = postingsEnum.freq();
                if (pattern.matcher(term).matches()) {
                    List<Integer> positions = getOffsetList(postingsEnum,termFreq);
                    statistics.add(new Statistic(i,term,positions));
                }
            }
        }

        return statistics;
    }

    private Pattern getPattern(String regex){ return  Pattern.compile(regex); }

    private IndexReader getIndexReader(File index) throws IOException {

        Path indexDirectory = index.toPath();
        IndexReader indexReader = DirectoryReader.open(FSDirectory.open(indexDirectory));

        return indexReader;
    }

    private List<Integer> getOffsetList(PostingsEnum postingsEnum, int freq) throws IOException {

        int offset;
        List<Integer> positionList = new ArrayList<>();

        for(int i = 0; i<freq; i++){
            postingsEnum.nextPosition();
            offset = postingsEnum.startOffset();
            positionList.add(offset);
        }

        return positionList;
    }
}
