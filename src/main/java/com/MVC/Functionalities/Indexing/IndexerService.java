package com.MVC.Functionalities.Indexing;

import com.MVC.Services.IOService;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Szymon on 22.11.2016.
 */


@Service
public class IndexerService {

    @Autowired
    private IOService ioService;

    private IndexWriter createIndexWriter(String username) throws IOException {

        IndexWriter indexWriter;

        //Index directory for given user
        Path indexDirectory = ioService.getIndexDir(username).toPath();

        FSDirectory dir = FSDirectory.open(indexDirectory);
        Analyzer analyzer = new WhitespaceAnalyzer();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);

        indexWriter = new IndexWriter(dir,config);

        return indexWriter;
    }

    public void indexFile(String username,String pathToFile) throws IOException {
        IndexWriter currentWriter = createIndexWriter(username);
        File fileToIndex = new File(pathToFile);

        /* Adding uploaded document */
        addDocument(currentWriter,fileToIndex);
        currentWriter.close();
    }

    private void addDocument(IndexWriter indexWriter, File file) throws IOException {

        System.out.println("Adding  document to index....");

        /* Create document */
        Document document = new Document();

        /* Specifies field types */
        FieldType fieldType = getFieldType();
        FileReader fileReader = new FileReader(file);

        /* Creates field with reader value */
        //document.add(getFileNameField("LOl"));
        document.add(new Field("text",fileReader,fieldType));

        /*Write to index */
        indexWriter.addDocument(document);
        System.out.println("File stored successfully in index!");
    }

    private StringField getFileNameField(String fileName) { return new StringField("fileName",fileName, Field.Store.YES); }

    private FieldType getFieldType(){
        FieldType fieldType = new FieldType();

        fieldType.setTokenized(true);
        fieldType.setStoreTermVectors(true);
        fieldType.setStoreTermVectorPositions(true);
        fieldType.setStoreTermVectorOffsets(true);
        fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);

        return fieldType;
    }
}
