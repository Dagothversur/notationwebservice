package com.MVC.Services;

import com.MVC.Dto.NotationDto;
import com.MVC.Functionalities.Search.Statistic;
import com.MVC.Model.Dao.ConfigRepositoryDAO;
import com.MVC.Model.entity.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by Szymon on 21.11.2016.
 */

@Service
public class IOService {

    private final static String SEPARATOR = File.separator;
    private final static String FILE_EXTENSION = ".txt";
    private final static String INDEX = "INDEX";
    private final static String STATISTIC = "Statistic.txt";

    @Autowired
    private ConfigRepositoryDAO configRepositoryDAO;

    public String saveFile(MultipartFile file,NotationDto notationDto) throws IOException {

        String uploadDirectory = getDestinationPath(notationDto);
        transferFile(uploadDirectory,file);

        return uploadDirectory;
    }

    public File getIndexDir(String user){
        Config sysConfig = configRepositoryDAO.getSystemConfig();
        String indexPath =  sysConfig.getDirectory() + user + SEPARATOR + INDEX;

        File f = new File(indexPath);
        if(!f.exists())
            f.mkdir();

        return f;
    }

    public File getDestinationFile(String user){
        Config sysConfig = configRepositoryDAO.getSystemConfig();

        String destinationPath = sysConfig.getDirectory() + user;
        return new File(destinationPath);
    }

    public String createStatisticFile(String username, List<Statistic> statistics) throws IOException {

        String insertedLine,insertedPositions;
        File destinationFile = getDestinationFile(username);
        String filePath = destinationFile.getAbsolutePath() + SEPARATOR + STATISTIC;

        File statisticFile = new File(filePath);
        FileWriter fw = new FileWriter(statisticFile);

        for(Statistic statistic : statistics){
            int docId = statistic.getDocId();
            String term = statistic.getTermMatched();
            List<Integer> positions = statistic.getPositions();
            insertedPositions = "";

            for(Integer position : positions)
                insertedPositions = insertedPositions + position + " ";

            insertedLine = "docId: " + docId + " term: " + term + " positions: |" + insertedPositions + "|\n";
            fw.write(insertedLine);
        }

        fw.close();

        return statisticFile.getAbsolutePath();
    }

    private String getDestinationPath(NotationDto notationDto){

        File destinationFile = getDestinationFile(notationDto.getUser());

        if(!destinationFile.exists())
            destinationFile.mkdir();

        return destinationFile.getAbsolutePath() + SEPARATOR + notationDto.getName() + FILE_EXTENSION;
    }

    /* Transfer uploaded file to our storage disc */
    private void transferFile(String uploadPath,MultipartFile file) throws IOException {

        File uploadFile = new File(uploadPath);
        file.transferTo(uploadFile);
    }
}
