package com.MVC.Services;

import com.MVC.Dto.EmailDto;
import com.MVC.Functionalities.passwordRecovery.IEmail;
import com.MVC.Utils.EmailUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by Simon on 2016-10-31.
 */
@Service
public class MailService {

    @Autowired
    private EmailUtils emailUtils;

    private static final String PASS_DESCRIPTION = "Password recovery";

    public void sendPasswordRecoveryMail(String to,String content){
        IEmail email = new EmailDto(to,PASS_DESCRIPTION,content);
        sendMail(email);
    }

    private void sendMail(IEmail email){

        String to = email.getDestinationMail();
        String from = emailUtils.getMailName();

        Session session = getSession(from);
        try {
            MimeMessage message=new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
            message.setHeader("Header","email.recovery");
            message.setSubject(email.getSubject());
            message.setText(email.getMailContent());

            Transport.send(message);
            System.out.println("[S] Email sent successfully.");
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }

    private Session getSession(String from) {
        Session session = Session.getDefaultInstance(getProperties(),
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(from, emailUtils.getPassword());
                    }
                });

        return session;
    }

    private Properties getProperties(){
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        /** Return gmail properties **/
        return props;
    }
}
