package com.MVC.validators;

import com.MVC.Dto.RegistrationFieldsDto;
import com.MVC.Model.Dao.CustomerRepositoryDAO;
import com.MVC.Model.Dao.DetailsRepositoryDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;


/**
 * Created by Simon on 2016-10-02.
 */
@Service
public class UniqueFieldsValidator implements Validator {

    @Autowired
    private DetailsRepositoryDAO detailsRepositoryDao;

    @Autowired
    private CustomerRepositoryDAO customerRepositoryDAO;

    @Override
    public boolean supports(Class<?> aClass) {
        return RegistrationFieldsDto.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        RegistrationFieldsDto uniqueFieldsDto = (RegistrationFieldsDto) o;

        if (o == null)
            errors.rejectValue("nullObject", "Passed object has null reference");
        if(!errors.hasErrors()) {
            ValidationUtils.rejectIfEmpty(errors, "username", "err.username", "Username field is empty");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "Email field is empty");
            if(errors.getFieldError("username") == null)
                validateUsername(uniqueFieldsDto, errors);
            if(errors.getFieldError("email") == null)
                validateEmail(uniqueFieldsDto, errors);
        }
    }

    private void validateEmail(RegistrationFieldsDto uniqueFieldsDto, Errors errors) {
        try {
            InternetAddress emailAddress = new InternetAddress(uniqueFieldsDto.getEmail());
            emailAddress.validate();
        } catch (AddressException e) {
            errors.rejectValue("email", "err.email", "Email field is not valid");
        }
        if(errors.getFieldError("email") == null && detailsRepositoryDao.findDetailsByEmail(uniqueFieldsDto.getEmail()) != null)
            errors.rejectValue("email","err.email","Email already exists");
    }

    private void validateUsername(RegistrationFieldsDto uniqueFieldsDto, Errors errors){

        int usernameLen = uniqueFieldsDto.getUsername().length();

        if(usernameLen < 6)
            errors.rejectValue("password","err.password","Username field is too short[" + usernameLen + "- current size,MIN=5]");
        if(usernameLen > 15)
            errors.rejectValue("password","err.password","Username field is too long[" + usernameLen + "- current size,MAX=20]");

        if(errors.getFieldError("username") == null && customerRepositoryDAO.findByUsername(uniqueFieldsDto.getUsername()) != null)
            errors.rejectValue("username", "err.username", "Username of that name already exists");
    }
}
