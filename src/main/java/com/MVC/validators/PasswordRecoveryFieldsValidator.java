package com.MVC.validators;

import com.MVC.Dto.PasswordRecoveryDto;
import com.MVC.Model.Dao.CustomerRepositoryDAO;
import com.MVC.Model.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Simon on 2016-11-01.
 */
@Service
public class PasswordRecoveryFieldsValidator implements Validator {

    @Autowired
    private CustomerRepositoryDAO customerRepositoryDAO;

    @Override
    public boolean supports(Class<?> aClass) {
        return PasswordRecoveryDto.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        PasswordRecoveryDto passwordRecoveryDto = (PasswordRecoveryDto) o;

        if(passwordRecoveryDto == null)
            errors.rejectValue("nullObject", "Passed object has null reference");
        if(!errors.hasErrors())
            ValidationUtils.rejectIfEmptyOrWhitespace(errors,"username","Username field is null");
        if(!errors.hasErrors())
            ValidationUtils.rejectIfEmptyOrWhitespace(errors,"email","Email field is null");
        if(!errors.hasErrors())
            validateCustomerExistence(passwordRecoveryDto.getUsername(),passwordRecoveryDto.getEmail(),errors);
    }

    private void validateCustomerExistence(String username,String email,Errors errors){
        Customer customer = customerRepositoryDAO.findByUsername(username);

        if(customer == null)
            errors.rejectValue("username","err.username","Username does not exist in the database");
        if(!errors.hasErrors()){
            boolean isEmailVailid = email.equals(customer.getDetails().getEmail());
            if(!isEmailVailid)
                errors.rejectValue("email","err.email","Email field is not assign to the given customer");
        }


    }
}
