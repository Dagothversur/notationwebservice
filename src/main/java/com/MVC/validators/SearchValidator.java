package com.MVC.validators;

import com.MVC.Dto.SearchDto;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Szymon on 08.12.2016.
 */

@Service
public class SearchValidator implements Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return SearchDto.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        SearchDto searchDto = (SearchDto) o;

        if(searchDto == null)
            errors.rejectValue("nullObject", "Passed object has null reference");
        if(!errors.hasErrors())
            ValidationUtils.rejectIfEmptyOrWhitespace(errors,"query","err.query","Query field is empty");
        if(!errors.hasErrors())
            validateQuery(searchDto,errors);
    }

    private void validateQuery(SearchDto searchDto,Errors errors){

        String query = searchDto.getQuery();
        int queryLen = query.length();

        if(queryLen <1)
            errors.rejectValue("query","err.query","Query length too short ");
        if(queryLen >15)
            errors.rejectValue("query","err.query","Query length too long");
        if(query.startsWith("*") && queryLen == 1)
            errors.rejectValue("query","err.query","Query should not contain only * keyword");
    }
}
