package com.MVC.validators;

import com.MVC.Dto.RegistrationFieldsDto;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Simon on 2016-10-10.
 */

@Service
public class RegistrationFieldsValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return RegistrationFieldsValidator.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        RegistrationFieldsDto registrationFieldsDto = (RegistrationFieldsDto) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"birthday","err.birthday","Birthday field is empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"password","err.password","Username field is empty");
        if(errors.getFieldError("password") == null)
            validatePassword(registrationFieldsDto,errors);
    }

    public void validatePassword(RegistrationFieldsDto registrationFieldsDto,Errors errors){

        int passwordLen = registrationFieldsDto.getPassword().length();

        if(passwordLen < 6)
            errors.rejectValue("password","err.password","Password field is too short[" + passwordLen + "- current size,MIN=6]");
        if(passwordLen > 15)
            errors.rejectValue("password","err.password","Password field is too long[" + passwordLen + "- current size,MAX=15]");
    }
}
