package com.MVC.validators;

import com.MVC.Model.Dao.ConfigRepositoryDAO;
import com.MVC.Model.entity.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Szymon on 21.11.2016.
 */
@Service
public class FileValidator implements Validator {

    @Autowired
    private ConfigRepositoryDAO configRepositoryDAO;

    @Override
    public boolean supports(Class<?> aClass) {
        return MultipartFile.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        MultipartFile file = (MultipartFile) o;
        Config config = configRepositoryDAO.getSystemConfig();

        if(file == null)
            errors.rejectValue("nullObject","Multipart file is null object");
        if(!errors.hasErrors())
            validateContentType(errors,file,config);
        if(!errors.hasErrors())
            validateContentSize(errors,file,config);
    }

    private void validateContentType(Errors errors, MultipartFile file, Config config){
        String contentType = file.getContentType();

        if(!contentType.equals(config.getFileExtension()))
            errors.rejectValue("fileItem","err.fileItem","File has invalid content type");
    }

    private void validateContentSize(Errors errors,MultipartFile file,Config config){

        Long sizeInBytes = file.getSize();

        if(sizeInBytes > config.getFileMaxSize())
            errors.rejectValue("size","err.size","Uploaded file size is invalid");
    }
}
