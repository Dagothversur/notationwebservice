package com.MVC.validators;

import com.MVC.Dto.NotationDto;
import com.MVC.Model.Dao.CustomerRepositoryDAO;
import com.MVC.Model.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Szymon on 17.11.2016.
 */
@Service
public class NotationValidator implements Validator {

    @Autowired
    private CustomerRepositoryDAO customerRepositoryDAO;

    @Override
    public boolean supports(Class<?> aClass) {
        return NotationDto.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        NotationDto notation = (NotationDto) o;

        if(notation == null)
            errors.rejectValue("nullObject","Notation is a null object");
        if(!errors.hasErrors())
            ValidationUtils.rejectIfEmptyOrWhitespace(errors,"user","User field is empty");
        if(!errors.hasErrors())
            ValidationUtils.rejectIfEmptyOrWhitespace(errors,"name","Name field is empty");
        if(!errors.hasErrors())
            ValidationUtils.rejectIfEmpty(errors,"encryptionRequested","Encryption field is empty");
        if(!errors.hasErrors())
            validateUser(errors,notation);
    }

    private void validateUser(Errors errors, NotationDto notationDto){
        Customer customer =   customerRepositoryDAO.findByUsername(notationDto.getUser());

        if(customer == null)
            errors.rejectValue("user","err.user","User does not exist in the database");
    }
}
