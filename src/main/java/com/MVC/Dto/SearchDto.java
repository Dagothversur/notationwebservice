package com.MVC.Dto;

import lombok.Data;

/**
 * Created by Szymon on 08.12.2016.
 */

@Data
public class SearchDto {

    private String query;
    private Boolean statistic;

}
