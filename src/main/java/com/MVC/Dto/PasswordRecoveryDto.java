package com.MVC.Dto;

import lombok.Data;

/**
 * Created by Simon on 2016-11-01.
 */
@Data
public class PasswordRecoveryDto {

    private String username;
    private String email;
}
