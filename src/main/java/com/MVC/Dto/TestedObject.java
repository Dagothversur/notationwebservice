package com.MVC.Dto;

import lombok.Data;

/**
 * Created by Simon on 2016-09-07.
 */
@Data
public class TestedObject {

    private String val;
}
