package com.MVC.Dto;

import com.MVC.Functionalities.passwordRecovery.IEmail;
import lombok.Data;

/**
 * Created by Simon on 2016-10-31.
 */
@Data
public class EmailDto implements IEmail {

    String destinationMail;
    String subject;
    String mailContent;

    public EmailDto(String destinationMail,String subject,String mailContent){
        this.destinationMail = destinationMail;
        this.subject = subject;
        this.mailContent = mailContent;
    }

}
