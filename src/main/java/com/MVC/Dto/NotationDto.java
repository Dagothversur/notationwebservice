package com.MVC.Dto;

import lombok.Data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by Simon on 2016-09-07.
 */
@Data
public class NotationDto {

    private String user;
    private String name;
    private Boolean encryptionRequested;
}
