package com.MVC.Dto;

import lombok.Data;
import org.springframework.validation.Errors;

import java.util.Date;

/**
 * Created by Simon on 2016-10-10.
 */

@Data
public class RegistrationResponseDto {

    private boolean hasErrors;
    private String usernameError;
    private String emailError;
    private String passwordError;
    private String birthdayError;

    public RegistrationResponseDto(){
        this.hasErrors = false;
        this.usernameError = null;
        this.emailError = null;
        this.passwordError = null;
        this.birthdayError = null;
    }
}
