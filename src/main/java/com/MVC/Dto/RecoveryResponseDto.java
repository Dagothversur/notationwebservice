package com.MVC.Dto;

import lombok.Data;

/**
 * Created by Simon on 2016-11-01.
 */
@Data
public class RecoveryResponseDto {

    private boolean isDataValid;
    private String response;

    public RecoveryResponseDto(){
        this.isDataValid = false;
        this.response = "No existing account for given username or email has been found";
    }
}
