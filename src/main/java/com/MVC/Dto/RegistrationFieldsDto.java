package com.MVC.Dto;

import lombok.Data;

import java.util.Date;

/**
 * Created by Simon on 2016-10-02.
 */

@Data
public class RegistrationFieldsDto {

    private String username;
    private String email;
    private String password;
    private Date birthday;
}
