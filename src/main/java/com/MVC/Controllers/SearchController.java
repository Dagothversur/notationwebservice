package com.MVC.Controllers;

import com.MVC.Dto.SearchDto;
import com.MVC.Functionalities.Search.SearchService;
import com.MVC.validators.SearchValidator;
import org.apache.commons.io.IOUtils;
import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Szymon on 16.11.2016.
 */
@Controller
public class SearchController {
    
    @Autowired
    private SearchService searchService;

    @Autowired
    private SearchValidator searchValidator;

    @RequestMapping( value = "/searchNotation", headers="Accept=application/json", method= RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<byte[]> search(@RequestBody SearchDto searchDto, BindingResult bindingResult) throws IOException, ParseException {

        String filePath;
        String user = "Dagothversur";
        searchValidator.validate(searchDto,bindingResult);

        if(!bindingResult.hasErrors()){
            filePath = searchService.performSearch(user,searchDto);

            System.out.println("Sending file...");

            return getResponseEntity(filePath);
        }

        return getResponseEntity(""); //refactor
    }


    private ResponseEntity<byte[]> getResponseEntity(String filePath) throws IOException {

        File response = new File(filePath);
        String fileName = "Statistic.txt";
        String type= response.toURL().openConnection().guessContentTypeFromName(fileName);

        InputStream inputStream = new FileInputStream(response);
        byte[] contents =  IOUtils.toByteArray(inputStream);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("content-disposition", "attachment; filename=" + fileName);
        responseHeaders.add("Content-Type",type);
        responseHeaders.setContentLength(Files.size(Paths.get(filePath)));

        return new ResponseEntity<>(contents, responseHeaders, HttpStatus.OK);
    }
}
