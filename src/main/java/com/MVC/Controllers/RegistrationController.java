package com.MVC.Controllers;

import com.MVC.Dto.RegistrationFieldsDto;
import com.MVC.Dto.RegistrationResponseDto;
import com.MVC.Functionalities.registration.RegistrationService;
import com.MVC.validators.RegistrationFieldsValidator;
import com.MVC.validators.UniqueFieldsValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * Created by Simon on 2016-10-02.
 */

@Controller
public class RegistrationController {

    @Autowired
    private UniqueFieldsValidator validator;

    @Autowired
    private RegistrationFieldsValidator registrationValidator;

    @Autowired
    private RegistrationService registrationService;

    @RequestMapping(value="/getUniqueFieldStatus",  headers="Accept=application/json", method= RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<RegistrationResponseDto> registerUser(@RequestBody RegistrationFieldsDto uniqueFieldsDto, BindingResult bindingResult){

        RegistrationResponseDto responseDto;

        validator.validate(uniqueFieldsDto,bindingResult);
        registrationValidator.validate(uniqueFieldsDto,bindingResult);

        if(bindingResult.hasErrors()) {
            responseDto = registrationService.createResponse(bindingResult);
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        }
        else
        {
            responseDto = new RegistrationResponseDto();
            registrationService.createNewAccount(uniqueFieldsDto);
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        }
    }

}
