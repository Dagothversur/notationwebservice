package com.MVC.Controllers;

import com.MVC.Dto.NotationDto;
import com.MVC.Dto.SearchDto;
import com.MVC.Functionalities.Indexing.IndexerService;
import com.MVC.Functionalities.Search.SearchService;
import com.MVC.Functionalities.encryption.EncryptionService;
import com.MVC.Services.IOService;
import com.MVC.validators.FileValidator;
import com.MVC.validators.NotationValidator;
import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


/**
 * Created by Simon on 2016-09-07.
 */

@Controller
public class UploadController {

    @Autowired
    private NotationValidator notationValidator;

    @Autowired
    private FileValidator fileValidator;

    @Autowired
    private IOService IOService;

    @Autowired
    private IndexerService indexerService;

    @Autowired
    private SearchService searchService;

    @Autowired
    private EncryptionService encryptionService;

    @RequestMapping(value = "/uploadNotation",headers = "Content-Type=multipart/form-data", method = RequestMethod.POST, produces = "application/json; text/plain;  charset=utf-8")
    @ResponseBody
    public String uploadNotation(@RequestPart("notationDto") NotationDto notationDto, @RequestPart("file") MultipartFile file) throws IOException, ParseException {

        BindingResult notationErrors = new BeanPropertyBindingResult(notationDto, "NotationDto");
        notationValidator.validate(notationDto, notationErrors);

        BindingResult fileErrors = new BeanPropertyBindingResult(file, "MultipartFile");
        fileValidator.validate(file, fileErrors);

        if (notationErrors.hasErrors()) {
            return "{\"failure\":1}";
        } else if (fileErrors.hasErrors()) {
            return "{\"failure\":1}";
        } else
        {
            String uploadedFile = IOService.saveFile(file,notationDto);
            indexerService.indexFile(notationDto.getUser(),uploadedFile);

            return "{\"success\":1}";
        }
    }

}


//            String uploadedFile = "C:\\Users\\Szymon\\Desktop\\DISC\\Dagothversur\\bible.txt";
//            String encryptedFile = "C:\\Users\\Szymon\\Desktop\\Nowy folder\\result.txt";
//            String decryptedFile = "C:\\Users\\Szymon\\Desktop\\Nowy folder\\result2.txt";
//
//            String key = "asdasdas";
//            encryptionService.encryptFile(key,uploadedFile,encryptedFile);
//            encryptionService.decryptFile(key,encryptedFile,decryptedFile);
