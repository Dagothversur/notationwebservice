package com.MVC.Controllers;

import com.MVC.Dto.PasswordRecoveryDto;
import com.MVC.Dto.RecoveryResponseDto;
import com.MVC.Functionalities.passwordRecovery.PasswordRecoveryService;
import com.MVC.validators.PasswordRecoveryFieldsValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Simon on 2016-11-01.
 */

@Controller
public class PasswordRecoveryController {

    @Autowired
    private PasswordRecoveryFieldsValidator validator;

    @Autowired
    private PasswordRecoveryService passwordRecoveryService;

    @RequestMapping(value = "/passwordRecovery", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<RecoveryResponseDto> recoverPassword(@RequestBody PasswordRecoveryDto passwordRecoveryDto, BindingResult bindingResult){

        validator.validate(passwordRecoveryDto,bindingResult);

        RecoveryResponseDto recoveryResponseDto = new RecoveryResponseDto();
        if(bindingResult.hasErrors())
            return new ResponseEntity<>(recoveryResponseDto,HttpStatus.OK);
        else
        {
            recoveryResponseDto.setDataValid(true);
            recoveryResponseDto.setResponse("Login data has been sent to given email");
            passwordRecoveryService.processMailData(passwordRecoveryDto);

            return new ResponseEntity<>(recoveryResponseDto, HttpStatus.OK);
        }
    }
}
